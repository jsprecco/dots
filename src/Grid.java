/*
 * Justin Sprecco
 * CSE 223, Spring 2023
 * May 22 2023
 * PA5
 * ** See doc for description **
 */

/**
 * Grid used to create an array of boxes based on the given grid size.
 * 
 * @author Justin Sprecco
 *
 */
public class Grid {
	private Box[][] boxGrid;		// box grid
	
	/**
	 * Constructs a grid based on the given grid size.
	 * 
	 * @param gridSize size of the grid
	 */
	public Grid(int gridSize) {
		boxGrid = new Box[gridSize - 1][gridSize - 1];
		for (int c=0; c < gridSize - 1; ++c) {
			for (int r=0; r < gridSize - 1; ++r) {
				boxGrid[c][r] = new Box();
			}
		}
	}
	
	/**
	 * Returns the box in the specified column and row.
	 * 
	 * @param c column of the requested box
	 * @param r row of the requested box
	 * @return the box in the specified row and column
	 */
	public Box getBox(int c, int r) {
		return boxGrid[c][r];
	}
}
