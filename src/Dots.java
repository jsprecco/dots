/*
 * Justin Sprecco
 * CSE 223, Spring 2023
 * May 22 2023
 * PA5
 * ** See doc for description **
 */
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

/**
 * <p>Dots is a two-player game where each player takes turns drawing an edge to a box. Once that box, or any adjacent boxes, is filled the player claims that box and earns a point for each box. The game ends when all the boxes are filled. The player with the most points at the end wins.</p>
 * 
 * @author Justin Sprecco
 *
 */
public class Dots extends JFrame {
	private GamePanel gamePane;			// Custom JFrame to paint the grid and register player moves
	private JTextField playerOneFld;	// input field for player 1
	private JTextField playerTwoFld;	// input filed for player 2
	private JLabel playerOneScoreLb;	// label for player 1 score
	private JLabel playerTwoScoreLb;	// label for player 2 score
	private JLabel infoLb;				// label for all output
	private JButton startBtn;			// button to (re)start the game
	private Player playerOne;			// Player object for player 1
	private Player playerTwo;			// Player object for player 2
	private boolean playerMove;			// true: player 1's move, false: player 2's move
	private boolean gameStarted;		// true: game is running, false: game is stopped

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Dots() {
		Font defaultFont = new Font("Avenir", Font.PLAIN, 30);		// font used for labels and fields
		EtchedBorder etchedBorder = new EtchedBorder(EtchedBorder.LOWERED, null, null);		// border used for fields and the game panel
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 825);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// main playing panel
		gamePane = new GamePanel();
		gamePane.setBounds(155, 155, 490, 490);
		gamePane.setBorder(etchedBorder);
		// listen for user input
		gamePane.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				playerMove(e);
			}
		});
		contentPane.add(gamePane);
		gamePane.setLayout(null);
				
		// top panel housing the player names and scores
		JPanel topPane = new JPanel();
		topPane.setLayout(null);
		topPane.setBounds(100, 10, 600, 130);
		contentPane.add(topPane);
		
		// player 1 label
		JLabel playerOneLb = new JLabel("Player 1");
		playerOneLb.setHorizontalAlignment(SwingConstants.CENTER);
		playerOneLb.setFont(defaultFont);
		playerOneLb.setBounds(20, 20, 195, 35);
		topPane.add(playerOneLb);
		
		// player 1 input field
		playerOneFld = new JTextField();
		playerOneFld.setHorizontalAlignment(SwingConstants.CENTER);
		playerOneFld.setFont(defaultFont);
		playerOneFld.setLocation(20, 75);
		playerOneFld.setSize(195, 40);
		playerOneFld.setBackground(null);
		playerOneFld.setBorder(etchedBorder);
		topPane.add(playerOneFld);
		
		// player 2 label
		JLabel playerTwoLb = new JLabel("Player 2");
		playerTwoLb.setHorizontalAlignment(SwingConstants.CENTER);
		playerTwoLb.setFont(defaultFont);
		playerTwoLb.setBounds(385, 20, 195, 35);
		topPane.add(playerTwoLb);
		
		// player 2 input field
		playerTwoFld = new JTextField();
		playerTwoFld.setHorizontalAlignment(SwingConstants.CENTER);
		playerTwoFld.setFont(defaultFont);
		playerTwoFld.setLocation(385, 75);
		playerTwoFld.setSize(195, 40);
		playerTwoFld.setBackground(null);
		playerTwoFld.setBorder(etchedBorder);
		topPane.add(playerTwoFld);
		
		// player 1 score label
		playerOneScoreLb = new JLabel("0");
		playerOneScoreLb.setHorizontalAlignment(SwingConstants.CENTER);
		playerOneScoreLb.setFont(defaultFont);
		playerOneScoreLb.setBounds(240, 30, 50, 75);
		topPane.add(playerOneScoreLb);
		
		// player 2 score label
		playerTwoScoreLb = new JLabel("0");
		playerTwoScoreLb.setHorizontalAlignment(SwingConstants.CENTER);
		playerTwoScoreLb.setFont(defaultFont);
		playerTwoScoreLb.setBounds(310, 30, 50, 75);
		topPane.add(playerTwoScoreLb);
		
		// bottom pane housing the output information and start button
		JPanel bottomPane = new JPanel();
		bottomPane.setBounds(100, 664, 600, 100);
		contentPane.add(bottomPane);
		bottomPane.setLayout(null);
		
		// information label
		infoLb = new JLabel();
		infoLb.setHorizontalAlignment(SwingConstants.CENTER);
		infoLb.setBounds(50, 0, 500, 25);
		infoLb.setFont(new Font("Avenir", Font.PLAIN, 22));
		infoLb.setText("");
		bottomPane.add(infoLb);
		
		// start button
		startBtn = new JButton("Start");
		startBtn.setFont(new Font("Avenir", Font.PLAIN, 22));
		startBtn.setLocation(175, 60);
		startBtn.setSize(250, 40);
		// listen for button press
		startBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				start();
			}
		});
		bottomPane.add(startBtn);
	}
	
	/**
	 * Start the Game.
	 */
	public void start() {
		// end the game if started
		gameStarted = false;
		// clear the grid in the result of a restart
		gamePane.clearGrid();

		char playerOneInitial;
		char playerTwoInitial;

		// ensure the users have entered their names
		try {
			playerOneInitial = playerOneFld.getText().charAt(0);
			playerTwoInitial = playerTwoFld.getText().charAt(0);
		} catch (Exception e) {
			infoLb.setText("Please enter player names.");
			return;
		}

		// ensure player names begin with a character
		if (!Character.isLetter(playerOneInitial) || !Character.isLetter(playerTwoInitial)) {
			infoLb.setText("Players must begin with a character constant");
			return;
		// ensure player initials are unique
		} else if (playerOneInitial == playerTwoInitial) {
			infoLb.setText("Players must have unique first initials");
			return;
		}

		// create the players
		playerOne = new Player(playerOneFld.getText());
		playerTwo = new Player(playerTwoFld.getText());
		
		startBtn.setText("Restart");

		// initialize the scoreboard
		playerOneScoreLb.setText("" + playerOne.getScore());
		playerTwoScoreLb .setText("" + playerTwo.getScore());
		
		// inform users of the current player's turn
		infoLb.setText(playerOne.getName() + "'s turn.");
		
		// player 1 gets the first turn
		playerMove = true;
		// start the game
		gameStarted = true;
	}
	
	/**
	 * <p>Player Move</p>
	 * @param e the mouse position after being pressed
	 */
	public void playerMove(MouseEvent e) {
		// ensure the game has started
		if (!gameStarted) {
			infoLb.setText("Click Start to begin");
			return;
		}

		// get the coordinates of the mouse press
		int x = e.getX();
		int y = e.getY();
			
		// determine which player made the move
		Player player = playerMove ? playerOne : playerTwo;

		// find the nearest edge
		int edge = gamePane.checkEdge(x, y);
		
		// if the edge has already been claimed
		if (edge < 0) {
			infoLb.setText("Edge already claimed");
			return;
		}

		// determine how many points the current player scored
		int score = gamePane.checkBox(x, y, edge, player);

		// update the score
		updateScore(player, score);
	
		// repaint after points scored
		gamePane.repaint();
	}
	
	/**
	 * Update the score.
	 * @param player the controlling player
	 * @param points the amount of points scored
	 */
	public void updateScore(Player player ,int points) {
		// if not points scored, next player's turn
		if (points == 0) {
			playerMove = !playerMove;
			player = playerMove ? playerOne : playerTwo;
			infoLb.setText(player.getName() + "'s turn.");
			return;
		// else, register the score and inform players
		} else {
			player.setScore(points);
			infoLb.setText(player.getName() + " Scored!");

			JLabel scoreLb = playerMove ? playerOneScoreLb : playerTwoScoreLb;
			scoreLb.setText("" + player.getScore());

			// check if all boxes are filled
			if (playerOne.getScore() + playerTwo.getScore() == 64) {
				gameOver();
			}
		}
	}
	
	/**
	 * End the game, announcing a winner.
	 */
	public void gameOver() {
		String info;
		
		// determine the winner
		if (playerOne.getScore() == playerTwo.getScore()) {
			info = "You finished in a tie!";
		} else if (playerOne.getScore() > playerTwo.getScore()) {
			info = playerOne.getName() + " is the winner!";
		} else {
			info = playerTwo.getName() + " is the winner!";
		}
		
		infoLb.setText(info);
		
		// stop the game
		gameStarted = false;
	}
}
