/*
 * Justin Sprecco
 * CSE 223, Spring 2023
 * May 22 2023
 * PA5
 * ** See doc for description **
 */

/**
 * Player holds the name and score for each player.
 * 
 * @author Justin Sprecco
 *
 */
public class Player {
	private String name; 
	private int score;
	
	/**
	 * Construct a player with the given name and an initial score of zero.
	 * 
	 * @param player player name
	 */
	public Player(String player) {
		name = player;
		score = 0;
	}
	
	/**
	 * Get player name.
	 * 
	 * @return player name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Get player score.
	 * 
	 * @return player's current score
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Set player score
	 * 
	 * @param points total points to add to player score
	 */
	public void setScore(int points) {
		score += points;
	}
}
