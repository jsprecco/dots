
/*
 * Justin Sprecco
 * CSE 223, Spring 2023
 * May 22 2023
 * PA5
 * ** See doc for description **
 */

/**
 * Box used in the grid used to determine if the edges are set and who owns it.
 * 
 * @author Justin Sprecco
 *
 */
public class Box {
	private boolean north;		// north edge
	private boolean east;		// east edge
	private boolean south;		// south edge
	private boolean west;		// west edge
	private Player owner;		// box owner
	
	/**
	 * Constructs the box with no owner and unset edges.
	 */
	public Box() {
		north = false;
		east = false;
		south = false;
		west = false;
	}
	
	/**
	 * Sets the specified edge.
	 * 
	 * @param edge specified edge
	 */
	public void setEdge(int edge) {
		switch (edge) {
		case 0:
			north = !north;
			break;
		case 1:
			east = !east;
			break;
		case 2:
			south = !south;
			break;
		case 3:
			west = !west;
			break;
		}
		
		return;
	}
	
	/**
	 * Gets the specified edge.
	 * 
	 * @param edge specified edge
	 * @return specified edge of this box
	 */
	public boolean getEdge(int edge) {
		boolean boxEdge = false;
		
		switch (edge) {
		case 0:
			boxEdge = north;
			break;
		case 1:
			boxEdge = east;
			break;
		case 2:
			boxEdge = south;
			break;
		case 3:
			boxEdge = west;
			break;
		}
		
		return boxEdge;
	}
	
	/**
	 * Determine if the box is complete.
	 * 
	 * @return true if box is complete, false if not
	 */
	public boolean isComplete() {
		if (north && east && south && west) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Set the owner.
	 * 
	 * @param player current player
	 */
	public void setOwner(Player player) {
		owner = player;
	}
	
	/**
	 * Get the owner.
	 * 
	 * @return owner of the box.
	 */
	public Player getOwner() {
		return owner;
	}
}
