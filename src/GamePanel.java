/*
 * Justin Sprecco
 * CSE 223, Spring 2023
 * May 22 2023
 * PA5
 * ** See doc for description **
 */

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JPanel;

/**
 * GamePanel is an extension of JPanel used for registering player moves and drawing the grid.
 * 
 * @author Justin Sprecco
 *
 */
public class GamePanel extends JPanel {
	private int gridSize;		// size of the grid
	private int cellWidth;		// width of each cell
	private int cellHeight;		// height of each cell
	private Grid grid;			// Grid object
	private int padding;		// padding used to create a nice appearance
	
	/**
	 * Constructs the playing grid
	 */
	public GamePanel() {
		gridSize = 9;
		cellWidth = 50;
		cellHeight = 50;
		padding = 42;
		grid = new Grid(gridSize);
	}
	
	/**
	 * Custom paint method to draw the grid and boxes.
	 */
	public void paint(Graphics g) {
		// use the JFrame paint method
		super.paint(g);
		// draw the dots
		drawGrid(g);
		// draw the edges
		drawEdges(g);
	}
	
	/**
	 * Find the nearest edge and determine if its been drawn.
	 * @param x the x-coordinate of the mouse
	 * @param y the y-coordinate of the mouse
	 * @return the nearest edge if not drawn, else a flag noting a claimed edge
	 */
	public int checkEdge(int x, int y) {
		// column where mouse was pressed
		int c = (x - padding) / cellWidth;
		// row where mouse was pressed
		int r = (y - padding) / cellHeight;
		// if mouse pressed outside the east/south borders column and row become the nearest inside the grid
		c = c > 7 ? 7 : c;
		r = r > 7 ? 7 : r;
		// northwest x-coordinate of current box
		int originX = c * cellWidth + padding;
		// northwest y-coordinate of current box
		int originY = r * cellHeight + padding;
		
		// mark the corners of the current box
		Point northWest = new Point(originX, originY);
		Point northEast = new Point(originX + cellWidth, originY);
		Point southEast = new Point(originX + cellWidth, originY + cellHeight);
		Point southWest = new Point(originX, originY + cellHeight);

		// mark the distances from current mouse press to current box corners
		double northWestDistance = northWest.distance(x, y);
		double northEastDistance = northEast.distance(x, y);
		double southEastDistance = southEast.distance(x, y);
		double southWestDistance = southWest.distance(x, y);
		
		// sum the distances of each point matching the corresponding edge
		double northEdgeDistance = northWestDistance + northEastDistance;
		double eastEdgeDistance = southEastDistance + northEastDistance;
		double southEdgeDistance = southWestDistance + southEastDistance;
		double westEdgeDistance = northWestDistance + southWestDistance;
		
		// array containing all edge distances
		double[] distances = {northEdgeDistance, eastEdgeDistance, southEdgeDistance, westEdgeDistance};;
		
		int edge = 0;
		
		// determine the smallest distance
		for (int i=1; i < 4; i++) {
			edge = distances[edge] < distances[i] ? edge : i;
		}
		
		// return the closest edge, else flag if already claimed
		return (grid.getBox(c, r).getEdge(edge) ? -1 : edge);
	}	
	
	/**
	 * Determine if the player's move resulted in points.
	 * @param x x-coordinate of the mouse
	 * @param y y-coordinate of the mouse
	 * @param edge
	 * @param player
	 * @return
	 */
	public int checkBox(int x, int y, int edge, Player player) {
		// column where mouse was pressed
		int c = (x - padding) / cellWidth;
		// row where mouse was pressed
		int r = (y - padding) / cellHeight;
		// if mouse pressed outside the east/south borders column and row become the nearest inside the grid
		c = c > 7 ? 7 : c;
		r = r > 7 ? 7 : r;

		// get the current box
		Box box = grid.getBox(c, r);
		// box adjacent to edge of current box
		Box box1;
		// total score from move
		int score = 0;

		// set the edge of current box
		box.setEdge(edge);

		// if box is complete assign current player as owner and give them a point
		if (box.isComplete()) {
			box.setOwner(player);
			++score;
		}

		/*
		 * Check the adjacent box, assigning an owner if complete, and adding to their total score
		 */
		if (edge == 0 && r > 0) {
			box1 = grid.getBox(c, r - 1);
			box1.setEdge(2);
			if (box1.isComplete()) {
				box1.setOwner(player);
				++score;
			}
		} else if (edge == 1 && c < gridSize - 2) {
			box1 = grid.getBox(c + 1, r);
			box1.setEdge(3);
			if (box1.isComplete()) {
				box1.setOwner(player);
				++score;
			}
		} else if (edge == 2 && r < gridSize - 2) {
			box1 = grid.getBox(c, r + 1);
			box1.setEdge(0);
			if (box1.isComplete()) {
				box1.setOwner(player);
				++score;
			}
		} else if (edge == 3 && c > 0) {
			box1 = grid.getBox(c - 1, r);
			box1.setEdge(1);
			if (box1.isComplete()) {
				box1.setOwner(player);
				++score;
			}
		}
				
		// return total score
		return score;
	}
	
	/**
	 * Clear the Grid.
	 */
	public void clearGrid() {
		grid = new Grid(gridSize);
		repaint();
	}
	
	/**
	 * Draw the Grid Dots.
	 * 
	 * @param g graphics
	 */
	public void drawGrid(Graphics g) {
		for (int c=0; c < gridSize; ++c) {
			for (int r=0; r < gridSize; ++r) {
				int x = c * cellWidth + padding - 2;
				int y = r * cellHeight + padding - 2;
				g.drawOval(x, y, 4, 4);		// draw the dot
				g.fillOval(x, y, 4, 4);		// fill the dot
			}
		}
	}
	
	/**
	 * Draw the Grid Edges.
	 * 
	 * @param g graphics
	 */
	public void drawEdges(Graphics g) {
		for (int c=0; c < gridSize - 1; ++c) {
			for (int r=0; r < gridSize - 1; ++r) {
				int x = c * cellWidth + padding;
				int y = r * cellHeight + padding;

				Box box = grid.getBox(c, r);

				/*
				 * Draw completed edges only
				 */
				if (box.getEdge(0)) {
					g.drawLine(x, y, x + cellWidth, y);
				}
				if (box.getEdge(1)) {
					g.drawLine(x + cellWidth, y + cellHeight, x + cellWidth, y);
				}
				if (box.getEdge(2)) {
					g.drawLine(x, y + cellHeight, x + cellWidth, y + cellHeight);
				}
				if (box.getEdge(3)) {
					g.drawLine(x, y, x, y + cellHeight);
				}
				// draw the owner's initials if complete
				if (box.isComplete()) {
					char[] owner = { box.getOwner().getName().charAt(0) };
					g.setFont(new Font("Avenir", Font.PLAIN, 30));
					g.drawChars(owner, 0, 1, x + 16, y + 35);
				}
			}
		}
	}
}
